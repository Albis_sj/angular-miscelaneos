import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html',
  styleUrls: ['./clases.component.css']
})
export class ClasesComponent implements OnInit {

  alerta: string = 'alerta exitosa';
  loading:boolean = false;

  propiedades: any = {
    error: false
  }
  constructor() { }

  ngOnInit(): void {
  }

  ejecutar(): void{
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }

}
